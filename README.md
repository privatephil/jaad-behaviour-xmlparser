# jaad-behaviour-xmlparser

An xml parser for behaviour data found in JAAD's behaviour-master files. This parser searches for pedestrian attributes where pedestrian is standing or walking and then creates a new xml with only those attributes.

-xml files in directory "files" will be read
-new xml files will be created in directory "outputs"