/*
 * Project Name: JAAD Behaviour XML Parser
 * Release Date: 17.04.2019
 * Version: 1.2
 * Author: Philipp von Kiedrowski
 * 
 */
package de.bv3.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Main {

	private static class PedestrianAction {
		private String startFrame;
		private String endFrame;
		private String action;

		public PedestrianAction(String startFrame, String endFrame, String action) {
			this.startFrame = startFrame;
			this.endFrame = endFrame;
			this.action = action;
		}

		public String getStartFrame() {
			return startFrame;
		}

		public String getEndFrame() {
			return endFrame;
		}

		public String getAction() {
			return action;
		}
	}

	private static String[] filesInDirectory;

	private static DocumentBuilderFactory factory;
	private static DocumentBuilder builder;
	private static File file;
	private static Document documentInput;

	private static Document documentOutput;
	private static String videoId;

	TransformerFactory transformerFactory;
	Transformer transformer;
	DOMSource domSource;
	StreamResult streamResult;

	private static ArrayList<ArrayList<PedestrianAction>> pedestrians;
	private static ArrayList<PedestrianAction> pedActions;
	
	private static boolean elaborated = false; // Sets more detailed attribute distinction when true
	
	public static void main(String[] args) {

		factory = DocumentBuilderFactory.newInstance();

		filesInDirectory = new File("files").list();

		for (String fileName : filesInDirectory) {

			System.out.println("Working on file: " + fileName);
			try {

				builder = factory.newDocumentBuilder();
				file = new File("files/" + fileName);
				documentInput = builder.parse(file);
				documentOutput = builder.newDocument();
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}

			Element root = documentInput.getDocumentElement();
			videoId = root.getAttribute("id");

			Element rootOutput = documentOutput.createElement("video");
			documentOutput.appendChild(rootOutput);
			rootOutput.setAttribute("id", videoId);

			// Take node "actions"
			NodeList actions = root.getElementsByTagName("actions");
			NodeList actionsChildNodes = actions.item(0).getChildNodes();

			// Iterate over child nodes of node "actions"
			boolean set = false;
			String startFrame = null;
			String endFrame = null;
			String action = null;
			int anzahlPedestrians = 0;
			NodeList pedestrianElements = null;
			Element pedestrianElement = null;
			Element actionElement = null;

			pedestrians = new ArrayList<ArrayList<PedestrianAction>>();

			for (int i = 0; i < actionsChildNodes.getLength(); i++) {
				System.out.print(".");

				if (actionsChildNodes.item(i).getNodeName().startsWith("pedestrian")) {

					// Take every child node from current pedestrian node
					pedestrianElements = actionsChildNodes.item(i).getChildNodes();

					pedActions = new ArrayList<PedestrianAction>();

					// Iterate over every child node from current pedestrian (actions)
					for (int k = 0; k < pedestrianElements.getLength(); k++) {

						System.out.print(".");
						if (pedestrianElements.item(k).getNodeType() == Node.ELEMENT_NODE) { // "isAction"

							NamedNodeMap pedAttributes = pedestrianElements.item(k).getAttributes();

							// Iterate over attributes of pedestrian's child node
							for (int p = 0; p < pedAttributes.getLength(); p++) {
								System.out.print(".");
								if (pedAttributes.item(p).getNodeName().startsWith("start_frame")) {
									startFrame = pedAttributes.item(p).getNodeValue();
								} else if (pedAttributes.item(p).getNodeName().startsWith("end_frame")) {
									endFrame = pedAttributes.item(p).getNodeValue();
								} else if (pedAttributes.item(p).getNodeName().startsWith("id")
										&& (pedAttributes.item(p).getNodeValue().equalsIgnoreCase("standing")
												|| pedAttributes.item(p).getNodeValue().equalsIgnoreCase("walking")
												|| pedAttributes.item(p).getNodeValue().equalsIgnoreCase("crossing")
												|| pedAttributes.item(p).getNodeValue().equalsIgnoreCase("slow down")
												|| pedAttributes.item(p).getNodeValue().equalsIgnoreCase("speed up"))) {
									if(elaborated) {
										action = pedAttributes.item(p).getNodeValue().toLowerCase().trim();
									} else {
										if(pedAttributes.item(p).getNodeValue().equalsIgnoreCase("standing")) {
											action = "standing";
										} else {
											action = "walking";
										}
									}
									
									set = true;
								}

							}

							// --------------

							if (set) {
								pedActions.add(new PedestrianAction(startFrame, endFrame, action));

								/*
								 * pedestrianElement = documentOutput.createElement("pedestrian" +
								 * anzahlPedestrians); rootOutput.appendChild(pedestrianElement);
								 * 
								 * Attr startFrameAttr = documentOutput.createAttribute("start_frame");
								 * startFrameAttr.setValue(startFrame);
								 * pedestrianElement.setAttributeNode(startFrameAttr);
								 * 
								 * Attr endFrameAttr = documentOutput.createAttribute("end_frame");
								 * endFrameAttr.setValue(endFrame);
								 * pedestrianElement.setAttributeNode(endFrameAttr);
								 * 
								 * Attr gaitAttr = documentOutput.createAttribute("gait");
								 * gaitAttr.setValue(gait); pedestrianElement.setAttributeNode(gaitAttr);
								 */
							}

							// --------------

							set = false;
						}

					}
					
					// ---

					if (pedActions != null && !pedActions.isEmpty()) {
						pedestrians.add(pedActions);
					}

					// ---
					
				}

			}

			// Write into XML Tree
			for (ArrayList<PedestrianAction> al : pedestrians) {
				anzahlPedestrians++;

				pedestrianElement = documentOutput.createElement("pedestrian" + anzahlPedestrians);
				rootOutput.appendChild(pedestrianElement);

				for (PedestrianAction ac : al) {
					actionElement = documentOutput.createElement("action");

					Attr startFrameAttr = documentOutput.createAttribute("start_frame");
					startFrameAttr.setValue(ac.getStartFrame());
					actionElement.setAttributeNode(startFrameAttr);

					Attr endFrameAttr = documentOutput.createAttribute("end_frame");
					endFrameAttr.setValue(ac.getEndFrame());
					actionElement.setAttributeNode(endFrameAttr);

					Attr actionAttr = documentOutput.createAttribute("action_frame");
					actionAttr.setValue(ac.getAction());
					actionElement.setAttributeNode(actionAttr);

					pedestrianElement.appendChild(actionElement);
				}

			}

			System.out.println("\nVideo ID: " + videoId);
			System.out.println("Number of found pedestrians: " + anzahlPedestrians);

			System.out.println("...writing file...");
			try {
				// Write the new xml file
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource domSource = new DOMSource(documentOutput);
				StreamResult streamResult = new StreamResult(new File("outputs/" + videoId + ".xml"));

				transformer.transform(domSource, streamResult);
			} catch (Exception e) {
				e.printStackTrace();
				System.exit(0);
			}

			System.out.println("---------------------------------");
		}

	}

}
